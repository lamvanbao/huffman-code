import tkinter


class View(tkinter.Tk):
    def __init__(self):
        super().__init__()
        
        self.lecode = tkinter.Label(master=self, text='Coded Text')
        self.lecode.grid(row=0, column=0)
        
        self.ldecode = tkinter.Label(master=self, text='Decoded Text')
        self.ldecode.grid(row=0, column=2)
        
        self.button = tkinter.Button(master=self, text='Encode')
        self.button.grid(row=0, column=1)
        
        self.encode = tkinter.Text(master=self)
        self.encode.grid(row=1, column=0)
        
        self.decode = tkinter.Text(master=self, state='disabled')
        self.decode.grid(row=1, column=2)